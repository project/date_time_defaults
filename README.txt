Date Time defaults widget

If you want to set default date and/or time for a date time field you can do this with this widget.

Instructions
1. Install module
2. Choose the field you want to have this widget on, go to form display and change widget to Data and Time defaults
3. Use the config and setup data and/or time